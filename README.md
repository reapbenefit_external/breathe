Reap Benefit, which works to empower youngsters and believes in building next-generation problem solvers, "Solve Ninjas", has developed a Plug & Play Air Quality station to monitor Air Quality, ‘BREATHE’ from anywhere. Air quality is an extremely complex problem, and it is difficult for common citizens to understand or gauge it. 

This *Do It Yourself (DIY) Learning product* is based on the idea of simplifying solutions and the problem in such a way that normal civilians can contribute efforts to solving local problems in & around air pollution. It provides suggestive data, and once the data is available, it becomes easy to take proper action with Government and Local Citizens.

## Contents:

#### **1. [What are you Breathing?](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/What-are-you-Breathing%3F)**
An overview of air quality and what it means.

#### **2. [Assembly Instructions](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Assembly-Instructions)**
An animated guide to assemble the DIY Air Quality Monitor.

#### **3. [Installation](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Installation)**
How to mount your Breathe Air Quality Monitor on the wall.

#### **4. [Hardware Life](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Hardware-Life)**
An estimate of the life of each component.

#### **5. [Our Testing Procedure](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Testing-Procedure)**
Highlights of our testing process.

#### **6. Actions and Impact**
Your everyday actions and their impact on the air quality around you.

#### **7. [Solutions](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Solutions)**
Actions you can take to better your local air quality.

#### **8. [Current work around air quality](https://gitlab.com/reapbenefit_external/breathe/wikis/Home/Current-work-around-Air-Quality)**


