#### Data

<!--- Type here what went wrong/ did not work for you. Example: "Reset not possible when USB cable plugged in"-->

#### Description

<!--- How did it go wrong? Example: "While the PCB was connected to my PC, the LCD screen displayed some random characters after uploading code. 
Even after switching on and off it did not work" 

OR

How do you suggest we can solve this issue? Example: "An intervention at the PCB level, 
where there is a reset switch on the PCB, as we are used to a reset switch on the PCB" -->


#### Impact

<!--- Who does this issue impact and how? Example: "It impacts Solve Ninjas as it is an additional step in their checklist, and creates additional confusion and friction in the user experience" -->

