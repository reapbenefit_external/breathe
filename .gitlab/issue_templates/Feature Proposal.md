#### Data

<!--- Type here what feature you would like to be added, in brief. Example: "Instruction to attach flow sensor to pipe."-->

#### Description

<!--- Describe how you would like this feature to be added. Example: "This instruction needs to be there in the Input Sensor wiki page of the Flow Sensor." -->


#### Impact

<!--- How would this proposed feature help you solve a problem/ help you achieve something? -->

#### What Next

<!--- How may we use this proposed feature for any other feature/issue that needs improvement? -->


/label ~"feature proposal"
