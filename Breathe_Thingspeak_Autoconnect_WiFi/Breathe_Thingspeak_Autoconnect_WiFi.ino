
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino 
#include <SoftwareSerial.h>
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <LiquidCrystal_I2C.h>


//Server Setup Section
//Uncomment the Channel and upload the code for respective channel.
//String apiKey = "Y5K724C0CHRLK757";//Channel 4
//String apiKey = "2K45P13J3P0G9I0U";//Channel 5
//String apiKey = "7X0WU2XP24ALDGY7";//Channel 6
//String apiKey = "JK00P3P7LETU4M1P";//Channel 7
String apiKey = "CPXGUNCUSEEL9LSD";//Channel 8
//String apiKey = "S95R73WX64B81L1S";//Channel 9
//String apiKey = "5W056N6CLEK11EZV";//Channel 10
//String apiKey = "WBR8J0R5HNRGLT8G";//Channel 11
//String apiKey = "CRK6YNPI7BT7R5B8";//Channel 12
//String apiKey = "MUCV57QB9K7UDBZ9";//Channel 13  , 0x27
//String apiKey = "AMQS1T8FMA3QM6IP";//Channel 14
//String apiKey = "ITXA07LPDANH07YI";//Channel 15
//String apiKey = "PHT7KQ2U90ISWGHC";//Channel 16
//String apiKey = "4GGGZO597H2R7M3E";//Channel 17 , 0x27
//String apiKey = "OTEZ3JAQGBMYZ7LV";//Channel 18 ,   0x27
//String apiKey = "492XIHU2JITA4VST";//Channel 19, 0x27
//String apiKey = "Y9B2USVR7W1CKW67";//Channel 20
//String apiKey = "ODHLZNADP0BDFW6F";//Channel 21
//String apiKey = "TZZHRPF62J6LQNTF";//Channel 22
//String apiKey = "QY6WEA8QD1WWJGBB";//Channel 23
//String apiKey = "5X8JKCV7OVKN1DYA";//Channel 26
//String apiKey = "";//Channel 25
const char* host = "139.59.69.168";


WiFiClient client;

//LCD
//LiquidCrystal_I2C lcd(0x3F, 16, 2); // check the lcd i2c number
LiquidCrystal_I2C lcd(0x27, 16, 2);

//Dust Sensor

#define DEBUG false
#define pin_rx D3
#define pin_tx D4

SoftwareSerial Device(pin_rx, pin_tx);

const int AutoSendOn[4] =     {0x68, 0x01, 0x40, 0x57};
const int AutoSendOff[4] =    {0x68, 0x01, 0x20, 0x77};
const int StartPmMeasure[4] = {0x68, 0x01, 0x01, 0x96};
const int StopPmMeasure[4] =  {0x68, 0x01, 0x02, 0x95};
const int ReadPm[4] =         {0x68, 0x01, 0x04, 0x93};

int isAutoSend = true;
int useReading = true;

int pm25 = 0;   // PM2.5
unsigned long lastReading = 0;

void sendCommand(const int *cmd) {
  int i;
  for (i = 0; i < 4; i++) {
    Device.write(cmd[i]);
  }
  // let a unicorn pass
  delay(10);
}

int readResponse(int l = 32) {
  int i = 0;
  int buf[l];

  unsigned long start = millis();

  while (Device.available() > 0 && i < l) {

    buf[i] = Device.read();                 // read bytes from device

    if (DEBUG) {
      Serial.print("i: "); Serial.print(i);
      Serial.print(" buf[i]: "); Serial.println(buf[i], HEX);
    }

    // check for HEAD or skip a byte
    if (i == 0 && !(buf[0] == 0x40 || buf[0] == 0x42 || buf[0] == 0xA5 || buf[0] == 0x96)) {
      if (DEBUG) {
        Serial.println("Skipping Byte");
      }
      continue;
    } else {
      i++;
    }

    if (buf[0] == 0x42 && buf[1] == 0x4d) { // Autosend
      if (DEBUG) {
        Serial.println("Autosend");
      }
      l = 32;
    }

    if (buf[0] == 0x40 && buf[2] == 0x4) {  // Reading
      if (DEBUG) {
        Serial.println("Reading");
      }
      l = 8;
    }

    if (buf[0] == 0xA5 && buf[1] == 0xA5) { // Pos. ACK
      if (DEBUG) {
        Serial.println("ACK");
      }
      return true;
    }

    if (buf[0] == 0x96 && buf[1] == 0x96) { // Neg. ACK
      if (DEBUG) {
        Serial.println("NACK");
      }
      return false;
    }

    if (millis() - start > 1000) {          // trigger Timeout after 1 sec
      Serial.println("Timeout");
      return false;
    }

  }

  // check checksum in Reading
  if (buf[2] == 0x04) {
    // HEAD+LEN+CMD
    int cs = buf[0] + buf[1] + buf[2];
    int c;

    // DATA
    for (c = 3; c < (2 + buf[1]); c++) {
      // Serial.println(buf[c]);
      cs += buf[c];
    }
    // CS = MOD((65536-(HEAD+LEN+CMD+DATA)), 256)
    cs = (65536 - cs) % 256;

    // validate checksum
    if (cs == buf[c]) {
      // calculate PM values
      pm25 = buf[3] * 256 + buf[4];
      return true;
    } else {
      Serial.println("Checksum mismatch");
    }
  } else if (buf[3] == 0x1c) { // Autoreading
    int cs = 0;
    int c;
    // DATA
    for (c = 0; c <= buf[3]; c++) {
      // Serial.println(buf[c]);
      cs += buf[c];
    }
    int checksum = buf[30] * 256 + buf[31];
    if (DEBUG) {
      Serial.print("Checksum: "); Serial.print(checksum, HEX);
      Serial.print(" CS: "); Serial.println(cs, HEX);
    }

    if (cs == checksum) {
      // calculate PM values
      pm25 = buf[6] * 256 + buf[7];
      return true;
    } else {
      Serial.println("Checksum mismatch");
    }
  } else {
    // unkown
  }

  return false;
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Device.begin(9600); Device.println();
  lcd.init();   // initializing the LCD
  lcd.backlight();
  lcd.print("Connect to Wifi");
  // WiFiManager
  WiFiManager wifiManager;
  wifiManager.setTimeout(180);

  if (!wifiManager.autoConnect("SetUpWifiOnBreathe")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }
  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd.clear();
  dustdata();
  showonlcd();
  senddata();
  delay(1000);

}

//Functions written now on
void dustdata() {


  if (millis() - lastReading >= 1000 || lastReading == 0) {
    lastReading = millis();

    // handle AutoSend
    if (isAutoSend) {
      if (useReading) {
        if (readResponse()) {
          Serial.print("PM 2.5: "); Serial.print(pm25);
        }
      } else { // disable
        Serial.println("Stop AutoSend");
        sendCommand(AutoSendOff);
        lcd.print("AutosendOff");
        if (readResponse()) {
          Serial.println("AutoSend disabled.");
          isAutoSend = !isAutoSend;
        }
      }
    } else {
      sendCommand(ReadPm);
      if (readResponse()) {
        Serial.print("PM 2.5: "); Serial.print(pm25);
      }
    }
  }
}
void senddata() {
  //WiFiClient client;
  const int httpPort = 3000; //Port 3000 is commonly used for www
  //---------------------------------------------------------------------
  //Connect to host, host(web site) is define at top
  if (!client.connect(host, httpPort)) {
    Serial.println("Connection Failed");
    delay(300);
    return; //Keep retrying until we get connected
  }
  String Link = "GET /update?api_key=" + apiKey + "&field1="; //Requeste webpage
  Link = Link + pm25;
  Link = Link + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n";
  client.print(Link);
  delay(100);

  //---------------------------------------------------------------------
  //Wait for server to respond with timeout of 5 Seconds
  int timeout = 0;
  while ((!client.available()) && (timeout < 1000))    //Wait 5 seconds for data
  {
    delay(10);  //Use this with time out
    timeout++;
  }

  //---------------------------------------------------------------------
  //If data is available before time out read it.
  if (timeout < 500)
  {
    while (client.available()) {
      Serial.println(client.readString()); //Response from ThingSpeak
    }
  }
  else
  {
    Serial.println("Request timeout..");
  }

  delay(50000);  //Read Web Page every 40 seconds
}

void showonlcd() {
  lcd.setCursor(0, 0);
  lcd.print("Breathing..");
  lcd.setCursor(0, 1);
  lcd.print("PM2.5:");
  lcd.print(pm25);
  delay(500);

  if (pm25 >= 0 && pm25 <= 75) {
    lcd.setCursor(10, 1);
    lcd.print("Good..");
    delay(1000);
  }    else if (pm25 >= 76 && pm25 <= 1000) {
    lcd.setCursor(10, 1);
    lcd.print("Bad....");
    delay(1000);
  }
}

